package ml.milvus.passformat.alphabet;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;

public enum AlphabetType {

	nato,
	douchebag,
	drseuss,
	none;

	public static AlphabetType fromName(final String name) {
		return Arrays.stream(values())
				.filter(v -> StringUtils.equals(v.name(), StringUtils.lowerCase(name)))
				.findFirst().orElse(none);
	}

}
