package ml.milvus.passformat.alphabet;

public class AlphabetException extends Throwable {

	public AlphabetException(final String message) {
		super(message);
	}

}
