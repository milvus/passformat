package ml.milvus.passformat.alphabet;

import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.TreeMap;
import java.util.stream.IntStream;

public class Alphabet {

	private final AlphabetType alphabetType;
	private final Map<Character, String> lettersMap;

	public Alphabet(final String name) throws AlphabetException {
		alphabetType = AlphabetType.fromName(StringUtils.trimToEmpty(name));
		lettersMap = new TreeMap<>();
		if (Objects.equals(alphabetType, AlphabetType.none)) {
			IntStream.rangeClosed('a', 'z').forEach(c -> lettersMap.put((char) c, Character.toString(c)));
		} else {
			String fileName = "alphabet/" + alphabetType.name();
			InputStream inputStream = Optional.ofNullable(getClass().getClassLoader().getResourceAsStream(fileName))
					.orElseThrow(() -> new AlphabetException("File '" + fileName + "' not found"));
			try (InputStreamReader inputStreamReader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
				 BufferedReader bufferedReader = new BufferedReader(inputStreamReader)) {
				for (String line; (line = bufferedReader.readLine()) != null; ) {
					String currentLine = StringUtils.lowerCase(StringUtils.trimToEmpty(line));
					if (StringUtils.isNotEmpty(currentLine) && StringUtils.length(currentLine) >= 1) {
						lettersMap.putIfAbsent(currentLine.charAt(0), currentLine);
					}
				}
			} catch (IOException e) {
				throw new AlphabetException("Exception during opening alphabet file: " + e.getMessage());
			}
		}
		Map<Character, String> uppercaseMap = new TreeMap<>();
		lettersMap.forEach((k, v) -> {
			uppercaseMap.putIfAbsent(Character.toUpperCase(k), StringUtils.upperCase(v));
		});
		lettersMap.putAll(uppercaseMap);
	}

	public String convertCharacter(final char c) {
		return lettersMap.getOrDefault(c, Character.toString(c));
	}

	public boolean isNone() {
		return Objects.equals(alphabetType, AlphabetType.none);
	}

	public Map<Character, String> getLettersMap() {
		return lettersMap;
	}

}
