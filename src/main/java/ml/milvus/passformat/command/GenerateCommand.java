package ml.milvus.passformat.command;

import lombok.SneakyThrows;
import ml.milvus.passformat.password.Default;
import ml.milvus.passformat.password.Password;
import ml.milvus.passformat.password.PasswordException;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

@Command(name = "generate", description = "Generate random password", version = "1.0", mixinStandardHelpOptions = true)
public class GenerateCommand extends AbstractInteractiveCommand {

	@Option(names = "--interactive", negatable = true, description = "Ask to input profile info from keyboard instead of command line arguments.")
	private boolean isInteractive;
	@Option(names = {"-q", "--quantity"}, description = "Number of passwords to generate (default: ${DEFAULT-VALUE}).", paramLabel = "<number>")
	private long quantity = 1;
	@Option(names = {"-l", "--length"}, description = "Password length (default: ${DEFAULT-VALUE}).")
	private int length = Default.LENGTH;
	@Option(names = "--no-uppercase", negatable = true, description = "Include uppercase letters (default: ${DEFAULT-VALUE}).")
	private boolean hasUppercase = Default.HAS_UPPERCASE;
	@Option(names = "--no-numbers", negatable = true, description = "Include numbers (default: ${DEFAULT-VALUE}).")
	private boolean hasNumbers = Default.HAS_NUMBERS;
	@Option(names = "--symbols", negatable = true, description = "Include symbols (default: ${DEFAULT-VALUE}).")
	private boolean hasSymbols = Default.HAS_SYMBOLS;
	@Option(names = "--similars", negatable = true, description = "Exclude similar-looking characters (i/I/1/l, o/O/0) (default: ${DEFAULT-VALUE}).")
	private boolean hasSimilars = Default.HAS_SIMILARS;
	@Option(names = "--no-duplicates", negatable = true, description = "Exclude duplicate characters from generated password (default: ${DEFAULT-VALUE}).")
	private boolean hasDuplicates = Default.HAS_DUPLICATES;
	@Option(names = "--other-symbols", description = "Other symbols to use in the password (default: ${DEFAULT-VALUE}).", paramLabel = "<other symbols>")
	private String otherSymbols = Default.DEFAULT_OTHER_SYMBOLS;

	@Override
	@SneakyThrows(PasswordException.class)
	protected List<String> getOutput() {
		if (isInteractive) {
			quantity = getTextIo().newLongInputReader()
					.withMinVal(1L)
					.withDefaultValue(1L)
					.read("Number of passwords to generate");
			length = getTextIo().newIntInputReader()
					.withDefaultValue(Default.LENGTH)
					.read("Password length");
			hasUppercase = getTextIo().newBooleanInputReader()
					.withDefaultValue(Default.HAS_UPPERCASE)
					.read("Include uppercase letters");
			hasNumbers = getTextIo().newBooleanInputReader()
					.withDefaultValue(Default.HAS_NUMBERS)
					.read("Include numbers");
			hasSymbols = getTextIo().newBooleanInputReader()
					.withDefaultValue(Default.HAS_SYMBOLS)
					.read("Include symbols");
			hasSimilars = getTextIo().newBooleanInputReader()
					.withDefaultValue(Default.HAS_SIMILARS)
					.read("Exclude similar-looking characters (i/I/1/l, o/O/0)");
			hasDuplicates = getTextIo().newBooleanInputReader()
					.withDefaultValue(Default.HAS_DUPLICATES)
					.read("Exclude duplicate characters from generated password");
			if (hasSymbols) {
				otherSymbols = getTextIo().newStringInputReader()
						.withDefaultValue(Default.DEFAULT_OTHER_SYMBOLS)
						.read("Other symbols to use in the password");
			}
		}
		Password password = Password.builder()
				.length(length)
				.hasUppercase(hasUppercase)
				.hasNumbers(hasNumbers)
				.hasSymbols(hasSymbols)
				.hasSimilars(hasSimilars)
				.hasDuplicates(hasDuplicates)
				.otherSymbols(otherSymbols).build();
		return LongStream.rangeClosed(1, quantity).mapToObj(s -> password.generate()).collect(Collectors.toList());
	}

}
