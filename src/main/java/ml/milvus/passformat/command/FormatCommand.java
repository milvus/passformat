package ml.milvus.passformat.command;

import lombok.SneakyThrows;
import ml.milvus.passformat.alphabet.Alphabet;
import ml.milvus.passformat.alphabet.AlphabetException;
import ml.milvus.passformat.alphabet.AlphabetType;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

import java.util.ArrayList;
import java.util.List;

@Command(name = "format", description = "Format password and credentials for storage", version = "0.99", mixinStandardHelpOptions = true)
public class FormatCommand extends AbstractInteractiveCommand {

	@Option(names = "--interactive", negatable = true, description = "Ask to input profile info from keyboard instead of command line arguments.")
	private boolean isInteractive;
	@Option(names = {"-p", "--password"}, description = "Password to format.", paramLabel = "<password>")
	private String password;
	@Option(names = {"-a", "--alphabet"}, description = "Phonetic alphabet for formatting.", paramLabel = "<alphabet>")
	private String alphabetName;
	@Option(names = {"-l", "--login"}, description = "Login for credentials.", paramLabel = "<login>")
	private String login;
	@Option(names = {"-e", "--email"}, description = "Email for credentials.", paramLabel = "<email>")
	private String email;
	@Option(names = {"-t", "--phone"}, description = "Phone number for credentials.", paramLabel = "<phone>")
	private String phone;
	@Option(names = {"-u", "--url"}, description = "URL for credentials.", paramLabel = "<url>")
	private String url;
	@Option(names = "--params", negatable = true, description = "Include additional parameters in key/value format.")
	private boolean hasParams;

	private final List<ImmutablePair<String, String>> additionalParams = new ArrayList<>();

	@Override
	@SneakyThrows(AlphabetException.class)
	protected List<String> getOutput() {
		final List<String> result = new ArrayList<>();
		if (isInteractive) {
			password = getTextIo().newStringInputReader()
					.withInputMasking(true)
					.read("Password to format");
			alphabetName = getTextIo().newEnumInputReader(AlphabetType.class)
					.withDefaultValue(AlphabetType.nato)
					.read("Phonetic alphabet for formatting")
					.name();
			login = getTextIo().newStringInputReader()
					.withMinLength(0)
					.withDefaultValue(login)
					.read("Login for credentials");
			email = getTextIo().newStringInputReader()
					.withMinLength(0)
					.withDefaultValue(email)
					.read("Email for credentials");
			phone = getTextIo().newStringInputReader()
					.withMinLength(0)
					.withDefaultValue(phone)
					.read("Phone number for credentials");
			url = getTextIo().newStringInputReader()
					.withMinLength(0)
					.withDefaultValue(url)
					.read("URL for credentials");
			hasParams = getTextIo().newBooleanInputReader()
					.withDefaultValue(false)
					.read("Include additional parameters in key/value format");
			if (hasParams) {
				String paramKey;
				String paramValue = "";
				do {
					paramKey = getTextIo().newStringInputReader()
							.withMinLength(0)
							.read("Name of additional parameter (leave blank to finish)");
					if (StringUtils.isNotEmpty(paramKey)) {
						paramValue = getTextIo().newStringInputReader()
								.read("Value of additional parameter");
					}
					additionalParams.add(new ImmutablePair<>(paramKey, paramValue));
				} while (StringUtils.isNotEmpty(paramKey));
			}
		}
		// format and output password
		result.add(password);
		Alphabet alphabet = new Alphabet(alphabetName);
		if (!alphabet.isNone()) {
			List<String> formattedCharacters = new ArrayList<>();
			for (Character c : password.toCharArray()) {
				formattedCharacters.add(alphabet.convertCharacter(c));
			}
			result.add(StringUtils.join(formattedCharacters, " "));
		}
		result.add(login);
		result.add(email);
		result.add(phone);
		result.add(url);
		// final output
		additionalParams.stream().filter(p -> StringUtils.isNotEmpty(p.getLeft())).forEach(p -> {
			result.add("[" + p.getLeft() + "]");
			result.add(p.getRight());
		});
		return result;
	}

}
