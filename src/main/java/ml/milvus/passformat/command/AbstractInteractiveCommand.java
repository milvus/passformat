package ml.milvus.passformat.command;

import org.apache.commons.lang3.StringUtils;
import org.beryx.textio.TextIO;
import org.beryx.textio.TextIoFactory;
import org.beryx.textio.TextTerminal;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

abstract class AbstractInteractiveCommand implements Runnable {

	private final TextIO textIo = TextIoFactory.getTextIO();
	private final TextTerminal terminal = textIo.getTextTerminal();
	private final List<String> output = new ArrayList<>();

	@Override
	public void run() {
		output.addAll(getOutput());
		terminal.init();
		terminal.println(output.stream().filter(StringUtils::isNotEmpty).collect(Collectors.toList()));
		terminal.dispose();
	}

	protected abstract List<String> getOutput();

	protected final TextIO getTextIo() {
		return textIo;
	}

}
