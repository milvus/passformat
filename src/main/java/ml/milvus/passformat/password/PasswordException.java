package ml.milvus.passformat.password;

public class PasswordException extends Throwable {

	public PasswordException(final String message) {
		super(message);
	}

}
