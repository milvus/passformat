package ml.milvus.passformat.password;

import lombok.Builder;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Password {

	private List<String> characters;

	@Builder.Default
	private int length = Default.LENGTH;
	@Builder.Default
	private boolean hasUppercase = Default.HAS_UPPERCASE;
	@Builder.Default
	private boolean hasNumbers = Default.HAS_NUMBERS;
	@Builder.Default
	private boolean hasSymbols = Default.HAS_SYMBOLS;
	@Builder.Default
	private boolean hasSimilars = Default.HAS_SIMILARS;
	@Builder.Default
	private boolean hasDuplicates = Default.HAS_DUPLICATES;
	@Builder.Default
	private String otherSymbols = Default.DEFAULT_OTHER_SYMBOLS;

	private final String ALL_SYMBOLS;
	private final String SIMILARS;

	@Builder
	public Password(final int length, final boolean hasUppercase, final boolean hasNumbers, final boolean hasSymbols, final boolean hasSimilars, final boolean hasDuplicates, final String otherSymbols) throws PasswordException {
		this.length = length;
		this.hasUppercase = hasUppercase;
		this.hasNumbers = hasNumbers;
		this.hasSymbols = hasSymbols;
		this.hasSimilars = hasSimilars;
		this.hasDuplicates = hasDuplicates;
		this.otherSymbols = otherSymbols;

		ALL_SYMBOLS = BooleanUtils.toString(this.hasSymbols, StringUtils.join(Default.STANDARD_SYMBOLS, this.otherSymbols), StringUtils.EMPTY);
		SIMILARS = BooleanUtils.toString(this.hasSimilars, StringUtils.EMPTY, Default.SIMILARS);

		initCharacters();

		if (!hasDuplicates && characters.size() < length) {
			throw new PasswordException("Not enough unique characters for the password with given length");
		}
	}

	public String generate() {
		StringBuilder result = new StringBuilder();
		List<String> unusedCharacters = new ArrayList<>(characters);
		Random random = new Random();
		IntStream.rangeClosed(1, length).forEach(s -> {
			String currentCharacter = unusedCharacters.get(random.nextInt(unusedCharacters.size()));
			result.append(currentCharacter);
			if (!hasDuplicates) {
				unusedCharacters.remove(currentCharacter);
			}
		});
		return result.toString();
	}

	private void initCharacters() {
		characters = new ArrayList<>();
		Set<ImmutablePair<Character, Character>> ranges = new HashSet<>();
		ranges.add(new ImmutablePair<>('a', 'z'));
		if (hasUppercase) {
			ranges.add(new ImmutablePair<>('A', 'Z'));
		}
		if (hasNumbers) {
			ranges.add(new ImmutablePair<>('0', '9'));
		}
		ranges.forEach(pair -> {
			characters.addAll(
					IntStream.rangeClosed(pair.getLeft(), pair.getRight())
							.filter(c -> !StringUtils.contains(SIMILARS, c))
							.mapToObj(Character::toString)
							.collect(Collectors.toList())
			);
		});
		char[] charArray = ALL_SYMBOLS.toCharArray();
		characters.addAll(
				IntStream.range(0, charArray.length)
						.mapToObj(c -> Character.toString(charArray[c]))
						.collect(Collectors.toSet())
		);
	}

	public String getAllCharacters() {
		return StringUtils.join(characters, StringUtils.EMPTY);
	}

}
