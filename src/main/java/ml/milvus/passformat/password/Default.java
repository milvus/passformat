package ml.milvus.passformat.password;

public interface Default {

	int LENGTH = 16;
	boolean HAS_UPPERCASE = true;
	boolean HAS_NUMBERS = true;
	boolean HAS_SYMBOLS = false;
	String STANDARD_SYMBOLS = "!@#$%^&*()+";
	String DEFAULT_OTHER_SYMBOLS = "~`[];?,";
	boolean HAS_SIMILARS = false;
	String SIMILARS = "iI1loO0";
	boolean HAS_DUPLICATES = true;

}
