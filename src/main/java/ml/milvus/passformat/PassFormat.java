package ml.milvus.passformat;

import ml.milvus.passformat.command.FormatCommand;
import ml.milvus.passformat.command.GenerateCommand;
import picocli.CommandLine;
import picocli.CommandLine.Command;

@Command(name = "PassFormat", subcommands = {
		FormatCommand.class,
		GenerateCommand.class,
		CommandLine.HelpCommand.class
})
public class PassFormat {

	public static void main(String[] args) {
		int exitCode = new CommandLine(new PassFormat()).execute(args);
		System.exit(exitCode);
	}

}
