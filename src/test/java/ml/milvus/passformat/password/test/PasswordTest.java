package ml.milvus.passformat.password.test;

import ml.milvus.passformat.password.Password;
import ml.milvus.passformat.password.PasswordException;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;

public class PasswordTest {

	private final String LOWERCASE = "abcdefghijklmnopqrstuvwxyz";
	private final String LOWERCASE_NO_SIMILARS = "abcdefghjkmnpqrstuvwxyz";

	@Test
	public void testLowercaseOnly() throws PasswordException {
		Password password = Password.builder()
				.hasSimilars(false)
				.hasNumbers(false)
				.hasSymbols(false)
				.hasUppercase(false)
				.build();
		Assert.assertEquals(LOWERCASE_NO_SIMILARS, password.getAllCharacters());
	}

	@Test
	public void testNotEnoughCharacters() {
		Assert.assertThrows(PasswordException.class, () -> Password.builder()
				.length(30)
				.hasUppercase(false)
				.hasNumbers(false)
				.hasDuplicates(false)
				.otherSymbols(StringUtils.EMPTY).build());
	}

}
