package ml.milvus.passformat.alphabet.test;

import ml.milvus.passformat.alphabet.Alphabet;
import ml.milvus.passformat.alphabet.AlphabetException;
import org.junit.Assert;
import org.junit.Test;

import java.util.Map;
import java.util.TreeMap;
import java.util.stream.IntStream;

public class AlphabetTest {

	@Test
	public void testNonexistentAlphabet() throws AlphabetException {
		final Map<Character, String> expected = new TreeMap<>();
		IntStream.rangeClosed('a', 'z').forEach(c -> expected.put((char) c, Character.toString(c)));
		IntStream.rangeClosed('A', 'Z').forEach(c -> expected.put((char) c, Character.toString(c)));
		Alphabet alphabet = new Alphabet("? DROP DATABASE");
		Assert.assertEquals(expected, alphabet.getLettersMap());
	}

	@Test
	public void testBloodhoundGangNatoLetters() throws AlphabetException {
		Alphabet alphabet = new Alphabet("nato");
		Assert.assertEquals("foxtrot", alphabet.convertCharacter('f'));
		Assert.assertEquals("UNIFORM", alphabet.convertCharacter('U'));
		Assert.assertEquals("charlie", alphabet.convertCharacter('c'));
		Assert.assertEquals("KILO", alphabet.convertCharacter('K'));
	}

	@Test
	public void testAbcDouchebagLetters() throws AlphabetException {
		Alphabet alphabet = new Alphabet("douchebag");
		Assert.assertEquals("ARMANI", alphabet.convertCharacter('A'));
		Assert.assertEquals("bentley", alphabet.convertCharacter('b'));
		Assert.assertEquals("chanel", alphabet.convertCharacter('c'));
	}

	@Test
	public void testXyzDrSeussLetters() throws AlphabetException {
		Alphabet alphabet = new Alphabet("drseuss");
		Assert.assertEquals("XTRAFOX", alphabet.convertCharacter('X'));
		Assert.assertEquals("yak", alphabet.convertCharacter('y'));
		Assert.assertEquals("zizzerzazzerzuzz", alphabet.convertCharacter('z'));
	}

}
