# About
Many people, myself included, generate random passwords for different sites and apps and store them in some kind of storage. The good news is that random passwords are pretty secure. The bad news is they are a pain to input manually (and sometimes you have to, like setting up a new smartphone, with nowhere to copy/paste from). The solution? Store a phonetic spelling of a password.

# Examples
`PassFormat format -i`

    xDvesUdFZXuJtTqF
    xray DELTA victor echo sierra UNIFORM delta FOXTROT ZULU XRAY uniform JULIETT tango TANGO quebec FOXTROT
    onyx87
    terry@online.net

`PassFormat generate -l 20 -d -m 5`

    7DG0EiK65X9IaoPLmlU1
    tGM8PiKYvNugcn6CXxw7
    Qz572eWjJLwZVi64UEOn
    MjN3pEbKABuIht7JWGaS
    B8P3Xky2JuYMQqLpDVhz
# Usage
## For formatting passwords and credentials info
Usage: PassFormat format \[-hirV\] \[--list-alphabets\] \[-p\[=<password>\]\]... \[-a=<alphabet>\] \[-e=<email>\] \[-l=<login>\] \[-t=<phone>\] \[-u=<url>\]  
Format password and credentials for storage  
-a, --alphabet=<alphabet>       Used alphabet to format.  
-e, --email=<email>             Email for credentials.  
-h, --help                      Show this help message and exit.  
-i, --interactive               Ask to input profile info from keyboard instead of command line arguments.  
-l, --login=<login>             Login for credentials.  
--list-alphabets                List all available alphabets.  
-p, --password\[=<password>\]   Password to format.  
-r, --params                    Include additional parameters in key/value format.  
-t, --phone=<phone>             Phone for credentials.  
-u, --url=<url>                 URL for credentials.  
-V, --version                   Print version information and exit.
## For generating random passwords
Usage: PassFormat generate \[-dhmnsuV\] \[-l=<length>\] \[-o=<other symbols>\] <number>  
Generate random password  
<number>                    Number of passwords to generate (default: 10).  
-d, --duplicates            Exclude duplicate characters from generated password (default: false).  
-h, --help                  Show this help message and exit.  
-l, --length=<length>       Password length (default: 16).  
-m, --similars              Exclude similar-looking characters (i/I/1/l, o/O/0) (default: true).  
-n, --numbers               Include numbers (default: true).  
-o, --other=<other symbols> Other symbols to use in the password (default: ~`[];?,).  
-s, --symbols               Include symbols (default: false).  
-u, --uppercase             Include uppercase letters (default: true).  
-V, --version               Print version information and exit.
# P.S.
All passwords in this file were randomly generated and are not used by me anywhere.
